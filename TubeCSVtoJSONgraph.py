import csv
import json

stations = {}

with open('London tube lines.csv', mode='r') as csv_file:
	csv_reader = csv.DictReader(csv_file)
	# Generate Unique Nodes
	for row in csv_reader:
		stations[row["From Station"]] = ""
		stations[row["To Station"]] = ""

output = (f'{{"nodes": [')

#	Generate Unique Nodes
for s in stations:
	#     {"id": "Abbey_Wood"},
	output += (f'{{"id": "{s}"}},')

#	Remove final comma
output = output[:-1]

output += "],"
output += '"links": ['

with open('London tube lines.csv', mode='r') as csv_file:
	csv_reader = csv.DictReader(csv_file)
	# Generate Unique Nodes
	for row in csv_reader:
		output += (f'{{"source":"{row["From Station"]}","target":"{row["To Station"]}", "line":"{row["Tube Line"]}"}},')

#	Remove final comma
output = output[:-1]

output += "]}"

print(output)