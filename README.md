# Force Directed London Tube Map

Force Directed Graph of TfL services.

This is a graph of the London Tube, Train, Tram, and DLR network - including CrossRail. Suitable for use with D3.

## Demo

Just the core TfL Services: https://edent.gitlab.io/force-directed-london-tube-map/force.html

## Graph of the London Tube, Train, Tram, and DLR network - including CrossRail

Download from: https://gitlab.com/edent/force-directed-london-tube-map/-/blob/master/TfL%20Graph.json

## Components

* Forked from https://gist.github.com/mapio/53fed7d84cd1812d6a6639ed7aa83868
* CSV of TfL stations and lines from https://www.doogal.co.uk/london_stations.php
* Official colours from http://content.tfl.gov.uk/tfl-colour-standards-issue04.pdf and https://oobrien.com/2012/01/tube-colours/
* D3JS v5 https://d3js.org/
* Crappy Python3 code to convert the CSV to JSON - all my fault

## Licence

Licence: gpl-3.0 https://www.gnu.org/licenses/gpl-3.0.en.html

## Keywords
D3, London Underground, Tube, CrossRail, Elizabeth Line, DLR.